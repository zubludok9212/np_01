import styles from "./Footer.module.css";
import { useCurrentDate } from "@kundinos/react-hooks";

const Footer = () => {
  const currentDate = useCurrentDate();
  // Получаем компоненты текущей даты
  const fullYear = currentDate.getFullYear();
  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <div className={styles.text}>
          <span className={`${styles.copyright} ${styles.bold}`}>
            © ООО «<span className="color-orange">Мой</span>Маркет», 2018-{`${fullYear}`}
          </span>
          <span className={styles.textPart}>
            Для уточнения информации звоните по номеру<span className={styles.nextLineMobile}></span> <a href="tel:+79000000000">+7 900 000 0000</a>,
          </span>
          <span className={styles.textPart}>
            а предложения по<span className={styles.nextLineMobile}></span> сотрудничеству отправляйте на почту<span className={styles.nextLineMobile}></span> <a href="mailto:partner@mymarket.com">partner@mymarket.com</a>
          </span>
        </div>
        <a className={styles.link} id="to-top-link" href="#top">Наверх</a>
      </div>
    </footer>
  );
};

export default Footer;