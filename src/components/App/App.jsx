import { Routes, Route } from 'react-router-dom';
import './App.css';
import { product } from '../../mocks/product';

import HomePage from '../../pages/HomePage/HomePage';
import ProductPage from '../../pages/ProductPage/ProductPage';
import NotFound from '../../pages/NotFound/NotFound'


const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route index element={<HomePage />}></Route>
        <Route path='product' element={<ProductPage product={product} />}></Route>
        <Route path='notFound' element={<NotFound />}></Route>
      </Routes>
    </div>
  );
}

export default App;
