import cn from 'classnames';

const ProductColorItem = ({ color, activeColor, setActiveColor }) => {
  const colorItemClasses = cn({
    'product-colors__item': true,
    'product-colors__item-active': color === activeColor,
  });

  return (
    <div 
      className={colorItemClasses} 
      onClick={() => { setActiveColor(color) }}
    >
      <img src={color.src} alt={color.alt} />
    </div>
  );
};

export default ProductColorItem;