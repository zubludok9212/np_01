import './ProductAdv.css';

const ProductAdv = () => {
  return (
    <div className="product-adv">
      <div className="product-adv__note">Реклама</div>
      <div className="product-adv__banners">
        <div className="product-adv__banner">Здесь могла бы быть ваша реклама</div>
        <div className="product-adv__banner">Здесь могла бы быть ваша реклама</div>
      </div>  
    </div>
  );
};

export default ProductAdv;