import { useState } from 'react';
import './ProductAddReview.css';

const ProductAddReview = () => {
  const [nameValue, setNameValue] = useState(localStorage.getItem('reviewName') || '');
  const [scoreValue, setScoreValue] = useState(localStorage.getItem('reviewScore') || '');
  const [textValue, setTextValue] = useState(localStorage.getItem('reviewText') || '');

  const [nameValueErrorStr, setNameValueErrorStr] = useState('');
  const [scoreValueErrorStr, setScoreValueErrorStr] = useState('');

  // Сброс информеров ошибок при повторном вводе после неуспешной валидации
  const handleFocusNameValue = () => {
    setNameValueErrorStr('');
    setScoreValueErrorStr('');
  };

  const handleFocusScoreValue = () => {
    setNameValueErrorStr('');
    setScoreValueErrorStr('');
  };


  // Изменение локальных стейтов полей (Контролируемый компонент)
  const handleInputNameValue = (e) => {
    localStorage.setItem('reviewName', e.target.value);
    setNameValue(e.target.value);
  };

  const handleInputScoreValue = (e) => {
    localStorage.setItem('reviewScore', e.target.value);
    setScoreValue(e.target.value);
  };

  const handleInputTextValue = (e) => {
    localStorage.setItem('reviewText', e.target.value);
    setTextValue(e.target.value);
  };

  // Валидация полей, set локальных стейтов для ошибок
  const validateName = () => {
    if (nameValue === '') {
      setNameValueErrorStr('Вы забыли указать имя и фамилию');
      return false;
    } else if (nameValue.length < 3) {
      setNameValueErrorStr('Имя не может быть короче двух символов');
      return false;
    } else {
      setNameValueErrorStr('');
      return true;
    }
  };

  const validateScore = () => {
    if (scoreValue === '') {
      setScoreValueErrorStr('Поле не заполнено: Оценка должна быть от 1 до 5');
      return false;
    } else if (!scoreValue.match(/^\d+$/)) {
      setScoreValueErrorStr('В поле введены буквы: Оценка должна быть от 1 до 5');
      return false;
    } else if (!scoreValue.match(/^[1-5]$/)) {
      setScoreValueErrorStr('Вы ввели цифры больше 5 или меньше 1: Оценка должна быть от 1 до 5');
      return false;
    } else {
      setScoreValueErrorStr('');
      return true;
    }
  };

  // Отправка формы
  const handleSubmitForm = (e) => {
    e.preventDefault();

    if(validateName() && validateScore()) {
      setNameValue('');
      setScoreValue('');
      setTextValue('');

      localStorage.removeItem('reviewName');
      localStorage.removeItem('reviewScore');
      localStorage.removeItem('reviewText');
      alert(`Ваш отзыв был успешно отправлен и будет отображён после модерации.`);
      console.log('Форма успешно отправлена');
    }
  };

  return (
    <section className="product-info-subsection addReview">
      <h4 className="addReview__title">Добавить свой отзыв</h4>

      <form className="addReview__form" onSubmit={handleSubmitForm}>      
        <div className="addReview__form-row addReview__form-row-responsive">
          <div className="addReview__form-nameWrapper">
            <input 
              type="text" 
              placeholder="Имя и фамилия" 
              className={nameValueErrorStr ? "addReview__form-field-error" : ""}
              value={nameValue}
              onInput={handleInputNameValue}
              onFocus={handleFocusNameValue}
            />
            {nameValueErrorStr &&
              <div className="addReview__form-error addReview__form-error-active">
                { nameValueErrorStr }
              </div>
            }
          </div>
          <div className="addReview__form-scoreWrapper">
            <input 
              type="text" 
              placeholder="Оценка" 
              className={!nameValueErrorStr && scoreValueErrorStr ? "addReview__form-field-error" : ""}
              value={scoreValue}
              onInput={handleInputScoreValue}
              onFocus={handleFocusScoreValue}
            />
            {!nameValueErrorStr && scoreValueErrorStr &&
              <div className="addReview__form-error addReview__form-error-active">
                { scoreValueErrorStr }
              </div>
            }
          </div>
        </div>
        <div className="addReview__form-row">
          <textarea
            placeholder="Текст отзыва" 
            name="review_text"
            value={textValue}
            onInput={handleInputTextValue}
          ></textarea>
        </div>
        <div className="addReview__form-row">
          <input type="submit" value="Отправить отзыв" />
        </div>
      </form>
    </section>

  );
};

export default ProductAddReview;
