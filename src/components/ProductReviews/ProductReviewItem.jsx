
const ProductReviewItem = ({ review }) => {

  return (
    <div className="reviews__item">
      <div className="reviews__item-left-column element-tablet">
        <img className="reviews__item-left-column-avatar" src={review.author.photo} alt={review.name} />
      </div>
      <div className="reviews__item-right-column">
        <div className="reviews__item-top">
          <div className="reviews__item-top-left element-mobile">
            <img className="reviews__item-avatar" src={review.author.photo} alt={review.author.name} />
          </div>
          <div className="reviews__item-top-right">
            <div className="reviews__item-title">{review.author.name}</div>
            <div className="reviews__item-stars">
              {review.rating >= 1 ? <img src="img/star-yellow.svg" alt="star1" /> : <img src="img/star-gray.svg" alt="star1" />}
              {review.rating >= 2 ? <img src="img/star-yellow.svg" alt="star2" /> : <img src="img/star-gray.svg" alt="star3" />}
              {review.rating >= 3 ? <img src="img/star-yellow.svg" alt="star3" /> : <img src="img/star-gray.svg" alt="star3" />}
              {review.rating >= 4 ? <img src="img/star-yellow.svg" alt="star4" /> : <img src="img/star-gray.svg" alt="star4" />}
              {review.rating === 5 ? <img src="img/star-yellow.svg" alt="star5" /> : <img src="img/star-gray.svg" alt="star5" />}
            </div>
          </div>
        </div>

        <div className="reviews__item-exp">
          <span className="bold">Опыт использования:</span> {review.experienceOfUsage}
        </div>

        <div className="reviews__item-prons">
          <div><span className="bold">Достоинства:</span></div>
          <div>
            {review.advantages}
          </div>
        </div>

        <div className="reviews__item-cons">
          <div><span className="bold">Недостатки:</span></div>
          <div>
            {review.disadvantages}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductReviewItem;