import { Link } from 'react-router-dom';
const Nav = () => {
  
  return (
    <nav className="nav">
      <div className="container">
      <Link to="/notFound" className="nav_a">Электроника</Link> {'>'} <Link to="/notFound" className="nav_a">Смартфоны и гаджеты</Link> {'>'} <Link to="/notFound" className="nav_a">Мобильные телефоны</Link> {'>'} <Link to="/notFound" className="nav_a">Apple</Link>
      </div>
    </nav>
  );
};

export default Nav;