import { Link } from 'react-router-dom';
import './NotFound.css';

import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Nav from "../../components/Nav/Nav";


const NotFound = () => {
  return (
    <>
      <Header />

      <Nav />
      <div className="container-content">
        <div className='not-found'>
          <div className="not-found__text">
            Здесь должно быть содержимое главной страницы.<br/>
            Но в рамках курса главная страница  используется лишь<br/>
            в демонстрационных целях<br/>
          </div>
          <Link className="homePage-link" to="/product">На страницу товара</Link>
        </div>
      </div>
      <Footer />
    </>
    );
};

export default NotFound;